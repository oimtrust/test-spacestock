<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Access Card_sc-iQtOjA iVYkEQ</name>
   <tag></tag>
   <elementGuidId>f46ec355-8b56-436c-aae3-e49b05a5de17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[3]/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/label/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>color</name>
      <type>Main</type>
      <value>--primary-color</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-iQtOjA iVYkEQ</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;app-inner&quot;]/div[@class=&quot;hide-on-mobile&quot;]/div[@class=&quot;filter-container&quot;]/div[@class=&quot;container filter-box&quot;]/div[@class=&quot;container filters&quot;]/div[@class=&quot;col-md-9 col-xs-12&quot;]/div[@class=&quot;row end-md&quot;]/div[@class=&quot;filter-facilities&quot;]/div[@class=&quot;facilities-dropdown&quot;]/div[@class=&quot;multiselect-dropdown&quot;]/div[@class=&quot;multiselect-content&quot;]/div[@class=&quot;row start-md&quot;]/div[@class=&quot;col-xs-12 col-md-12&quot;]/div[@class=&quot;opt-item&quot;]/div[@class=&quot;checkbox-wrapper&quot;]/label[@class=&quot;checkbox-wrapper__label&quot;]/div[@class=&quot;sc-jrIrqw hRfUJd&quot;]/div[@class=&quot;sc-iQtOjA iVYkEQ&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[3]/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div[2]/div/label/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Access Card'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Created with Sketch.'])[2]/following::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Atm Center'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basketball Court'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/label/div/div</value>
   </webElementXpaths>
</WebElementEntity>
