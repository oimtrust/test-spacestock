<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>polyline</name>
   <tag></tag>
   <elementGuidId>135e9550-ffb1-4494-bff3-f1161a551597</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>polyline</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>points</name>
      <type>Main</type>
      <value>20 6 9 17 4 12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;app-inner&quot;]/div[@class=&quot;hide-on-mobile&quot;]/div[@class=&quot;filter-container&quot;]/div[@class=&quot;container filter-box&quot;]/div[@class=&quot;container filters&quot;]/div[@class=&quot;col-md-9 col-xs-12&quot;]/div[@class=&quot;row end-md&quot;]/div[@class=&quot;filter-facilities&quot;]/div[@class=&quot;facilities-dropdown&quot;]/div[@class=&quot;multiselect-dropdown&quot;]/div[@class=&quot;multiselect-content&quot;]/div[@class=&quot;row start-md&quot;]/div[@class=&quot;col-xs-12 col-md-12&quot;]/div[@class=&quot;opt-item with-bg&quot;]/div[@class=&quot;checkbox-wrapper&quot;]/label[@class=&quot;checkbox-wrapper__label&quot;]/div[@class=&quot;sc-jrIrqw hRfUJd&quot;]/div[@class=&quot;sc-iQtOjA kbjnzW&quot;]/svg[@class=&quot;sc-hjRWVT jiKnyQ&quot;]/polyline[1]</value>
   </webElementProperties>
</WebElementEntity>
