<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_BBQ Pit_sc-hjRWVT jiKnyQ</name>
   <tag></tag>
   <elementGuidId>bdcf79dc-a99f-4ab1-9df0-0a0813541859</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='BBQ Pit'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sc-hjRWVT jiKnyQ</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;app-inner&quot;]/div[@class=&quot;hide-on-mobile&quot;]/div[@class=&quot;filter-container&quot;]/div[@class=&quot;container filter-box&quot;]/div[@class=&quot;container filters&quot;]/div[@class=&quot;col-md-9 col-xs-12&quot;]/div[@class=&quot;row end-md&quot;]/div[@class=&quot;filter-facilities&quot;]/div[@class=&quot;facilities-dropdown&quot;]/div[@class=&quot;multiselect-dropdown&quot;]/div[@class=&quot;multiselect-content&quot;]/div[@class=&quot;row start-md&quot;]/div[@class=&quot;col-xs-12 col-md-12&quot;]/div[@class=&quot;opt-item with-bg&quot;]/div[@class=&quot;checkbox-wrapper&quot;]/label[@class=&quot;checkbox-wrapper__label&quot;]/div[@class=&quot;sc-jrIrqw hRfUJd&quot;]/div[@class=&quot;sc-iQtOjA kbjnzW&quot;]/svg[@class=&quot;sc-hjRWVT jiKnyQ&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BBQ Pit'])[1]/following::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basketball Court'])[1]/following::*[name()='svg'][2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drug Store'])[1]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Function Hall'])[1]/preceding::*[name()='svg'][2]</value>
   </webElementXpaths>
</WebElementEntity>
